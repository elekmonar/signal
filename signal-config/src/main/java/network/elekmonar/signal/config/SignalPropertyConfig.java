package network.elekmonar.signal.config;

import java.io.Serializable;

import org.apache.deltaspike.core.api.config.PropertyFileConfig;

/**
 * Определяет конфигурационный файл для чтения из него
 * свойств приложения во время исполнения приложения.
 * 
 * @author Vitaly Masterov
 * @since 0.1
 * @see PropertyFileConfig
 *
 */
public class SignalPropertyConfig implements PropertyFileConfig, Serializable {

	private static final long serialVersionUID = -7929227818495734931L;

	private static final String WF_CONFIG_DIR = "jboss.server.config.dir";
	
	private static final String CONFIG_FILE_NAME = "signal.properties";
	
	@Override
	public String getPropertyFileName() {
		return System.getProperty(WF_CONFIG_DIR) + "/apps/" + CONFIG_FILE_NAME;
	}

	@Override
	public boolean isOptional() {
		return false;
	}

}
