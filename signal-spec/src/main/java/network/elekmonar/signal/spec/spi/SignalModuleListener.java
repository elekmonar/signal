package network.elekmonar.signal.spec.spi;

import javax.ejb.Local;
import javax.jms.Message;

/**
 * Интерфейс, использующийся в каждом модуле {@literal ontospace},
 * служит для реализации приёмника сигнальных сообщений.
 * 
 * @author Vitaly Masterov
 * @since 0.1
 *
 */
@Local
public interface SignalModuleListener {
	
	public void onMessage(Message message);

}