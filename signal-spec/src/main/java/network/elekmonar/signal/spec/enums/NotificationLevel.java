package network.elekmonar.signal.spec.enums;

/**
 * Перечисление, описывающее уровни
 * сообщений для придания сообщению статуса важности.
 * 
 * @author Vitaly Masterov
 * @since 0.11
 *
 */
public enum NotificationLevel {
	
	TRACE("trace"),
	
	DEBUG("debug"),

	INFO("info"),
	
	WARNING("warning"),
	
	ERROR("error"),
	
	FATAL("fatal");
	
	private String name;
	
	NotificationLevel(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}
	
}