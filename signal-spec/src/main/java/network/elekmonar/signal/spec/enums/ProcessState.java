package network.elekmonar.signal.spec.enums;

/**
 * Возможные статусы процесса.
 * 
 * @author Vitaly Masterov
 * @since 0.4
 *
 */
public enum ProcessState {
	
	/**
	 * Запланирован к выполнению
	 */
	PLANNED,
	
	/**
	 * В процессе
	 */
	IN_PROGRESS,
	
	/**
	 * Приостановлен
	 */
	STOPPED,
	
	/**
	 * Отменён (выведен из статуса {@literal PLANNED}
	 */
	CANCELED,
	
	/**
	 * Оборван (выведен из статуса {@literal IN_PROGRESS}
	 */
	INTERRUPTED,
	
	/**
	 * Во время выполнения процесса произошла ошибка,
	 * выполнение процесса было прервано.
	 */
	FAILED,
	
	/**
	 * Завершён
	 */
	DONE;

}