package network.elekmonar.signal.spec;

/**
 * Параметр сигнала
 * 
 * @author Vitaly Masterov
 * @since 0.6
 *
 */
public interface SignalParameter {
	
	/**
	 * Выдать ключ параметра
	 * @return строковое значение ключа параметра
	 */
	public String getKey();
	
	/**
	 * Выдать значение параметра
	 * @return значение параметра, представленное соответствующим объектом 
	 */
	public Object getValue();

}