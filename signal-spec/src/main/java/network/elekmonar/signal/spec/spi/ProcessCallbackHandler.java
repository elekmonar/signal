package network.elekmonar.signal.spec.spi;

public interface ProcessCallbackHandler<T> {

	public void doHandle(ProcessSignalContext context, T data);
	
}