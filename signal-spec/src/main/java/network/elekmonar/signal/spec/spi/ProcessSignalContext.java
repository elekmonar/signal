package network.elekmonar.signal.spec.spi;

import java.io.Serializable;
import java.util.UUID;

import network.elekmonar.signal.spec.ControlSignal;
import network.elekmonar.signal.spec.enums.ProcessState;

/**
 * Интерфейс, определяющий контекст процесса,
 * связанный с ранее поступившим управляющим сигналом.
 * Процесс выполняется в рамках этого контекста, соответственно
 * неразрывно связан с ним.
 * В этом интерфейсе определены методы посылки сигналов следующих типов:
 * <ul>
 * 	<li>Сигнал об ошибке</li>
 * 	<li>Сигнал о состоянии процесса</li>
 * 	<li>Сигнал о прогрессе выполнения</li>
 * 	<li>Сигнал с нотификацией</li>
 * 	<li>Сигнал с результатом выполнения</li>
 * </ul>
 * 
 * @author Vitaly Masterov
 * @since 0.1
 * @see NotificationSignal
 * @see ProcessState  
 *
 */
public interface ProcessSignalContext {
	
	/**
	 * Получить идентификатор экземпляра процесса
	 * 
	 * @return значение типа UUID
	 */
	public UUID getProcessId();
		
	/**
	 * Получить управляющий сигнал, ассоциированный
	 * с данным процессом. По сути тот сигнал, который
	 * явился фактором запуса данного процесса.
	 * 
	 * @return ControlSignal
	 */
	public ControlSignal getControlSignal();
	
	/**
	 * Отправить управляющий сигнал
	 * 
	 * @param signal Управляющий сигнал
	 */
	public void sendControlSignal(ControlSignal signal);
	
	/**
	 * Послать сигнал об ошибке
	 * 
	 * @param code Код ошибки
	 */
	public void sendError(String code);
	
	/**
	 * Послать сигнал об ошибке, приложив при этом
	 * исключение, которое возникло при выполнении кода.
	 * 
	 * @param code Код ошибки из реестра
	 * @param e Исключение
	 */
	public void sendError(String code, Exception e);
	
	/**
	 * Послать сигнал о текущем состоянии процесса
	 * 
	 * @param state Состояние процесса
	 */
	public void sendState(ProcessState state);
	
	/**
	 * Послать оповещение о завершении 
	 * выполнения процесса
	 */
	public void sendDone();

	/**
	 * Послать сигнал о прогрессе выполнения процесса
	 * 
	 * @param value Прогресс выполнения в процентах
	 */
	public void sendProgress(Integer value);
		
	/**
	 * Послать сообщение с результатом вычисления
	 * работы процесса
	 * 
	 * @param result Результат выполнения процесса
	 */
	public void sendResult(Serializable result);
	
}