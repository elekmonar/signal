package network.elekmonar.signal.spec;

import java.util.UUID;

import network.elekmonar.signal.spec.enums.ProcessState;

/**
 * Сигнал, извещающий о текущем состоянии процесса.
 * 
 * @author Vitaly Masterov
 * @since 0.4
 *
 */
public interface StateSignal {

	/** 
	 * @return Идентификатор процесса
	 */
	public UUID getProcess();
	
	/**
	 * 
	 * @param id Идентификатор процесса
	 */
	public void setProcess(UUID id);
	
	/** 
	 * @return Текущее состояние процесса
	 */
	public ProcessState getState();
	
	/**
	 * 
	 * @param state Текущее состояние процесса
	 */
	public void setState(ProcessState state);
	
}