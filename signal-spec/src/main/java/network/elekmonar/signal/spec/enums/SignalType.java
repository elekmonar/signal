package network.elekmonar.signal.spec.enums;

import java.util.HashMap;
import java.util.Map;

import network.elekmonar.signal.spec.ControlSignal;
import network.elekmonar.signal.spec.ErrorSignal;
import network.elekmonar.signal.spec.ProgressSignal;
import network.elekmonar.signal.spec.StateSignal;

/**
 * Типы сигналов
 * 
 * @author Vitaly Masterov
 * @since 0.1
 *
 */
public enum SignalType {

	/**
	 * Управляющий сигнал
	 */
	CONTROL(ControlSignal.class),

	/**
	 * Сигнал текущего статуса процесса
	 */
	STATE(StateSignal.class),

	/**
	 * Сигнал прогресса выполнения процесса
	 */
	PROGRESS(ProgressSignal.class),

	/**
	 * Сигнал об ошибке выполнения процесса
	 */
	ERROR(ErrorSignal.class);

	private static Map<Class<?>, SignalType> signalTypes;

	private Class<?> signalClass;

	public static final String SIGNAL_TYPE = "SignalType";

	static {
		signalTypes = new HashMap<>();
		signalTypes.put(ControlSignal.class, CONTROL);
		signalTypes.put(StateSignal.class, STATE);
		signalTypes.put(ProgressSignal.class, PROGRESS);
		signalTypes.put(ErrorSignal.class, ERROR);
	}

	SignalType(Class<?> signalClass) {
		this.signalClass = signalClass;
	}

	public Class<?> getSignalClass() {
		return signalClass;
	}

	public static SignalType fromSignalClass(Class<?> signalClass) {
		return signalTypes.get(signalClass);
	}

}