package network.elekmonar.signal.spec.enums;

public enum NotificationPriority {
	
	LOWEST,
	
	LOW,
	
	MEDIUM,
	
	HIGH,
	
	HIGHEST;

}