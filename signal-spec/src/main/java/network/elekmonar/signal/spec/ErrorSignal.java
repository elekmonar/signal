package network.elekmonar.signal.spec;

import java.util.UUID;

public interface ErrorSignal {
	
	public UUID getProcess();
	
	public void setProcess(UUID process);
	
	public String getCode();
	
	public void setCode(String code);
	
	public byte[] getStackTrace();
	
	public void setStackTrace(byte[] stackTrace);

}