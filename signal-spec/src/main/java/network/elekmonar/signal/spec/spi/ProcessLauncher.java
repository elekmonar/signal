package network.elekmonar.signal.spec.spi;

/**
 * Интерфейс, который описывает точку запуска регламентых 
 * процессов и задач. Его должны реализовывать классы, 
 * помеченные квалификатором {@link @Named} с указанием 
 * идентификатора задачи из реестра регламентных процессов. 
 * Каждый такой класс-реализатор задаёт свою бизнес-логику 
 * выполнения процесса и как результат может формировать 
 * результат выполнения процесса с последующей его передачей 
 * модулю {@literal ontospace-signal}.
 * 
 * @author Vitaly Masterov
 * @since 0.1
 *
 */
public interface ProcessLauncher {

	/**
	 * 
	 * @param context
	 */
	public void launch(ProcessSignalContext context);

}