package network.elekmonar.signal.spec.annotations;

public @interface ProcessCallbacks {
	
	ProcessCallback[] value();

}