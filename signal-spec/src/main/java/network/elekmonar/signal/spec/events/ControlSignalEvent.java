package network.elekmonar.signal.spec.events;

import java.io.Serializable;

import network.elekmonar.signal.spec.ControlSignal;

public class ControlSignalEvent implements Serializable {

	private static final long serialVersionUID = 4531131505805318515L;
	
	private ControlSignal signal;
	
	public ControlSignalEvent(ControlSignal signal) {
		this.signal = signal;
	}

	public ControlSignal getSignal() {
		return signal;
	}

	public void setSignal(ControlSignal signal) {
		this.signal = signal;
	}		

}