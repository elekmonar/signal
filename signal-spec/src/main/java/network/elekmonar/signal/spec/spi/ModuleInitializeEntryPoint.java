package network.elekmonar.signal.spec.spi;

import network.elekmonar.signal.spec.ControlSignal;
import network.elekmonar.signal.spec.sender.ControlSignalSender;

/**
 * Интерфейс который нужно реализовать в случае необходимости
 * запуска процедуры инициализации заданного модуля.
 * Схема работы следующая:
 * Прежде всего производится инициализация сигнального модуля и
 * только после этого сигнальный модуль выполняет массовую рассылку
 * служебного сообщения другим модулям. Метод {@literal entryPoint}
 * класса, реализующего этот интерфейс, запускается в ответ на получение
 * служебного сообщения от сигнального модуля. В реализации данного метода
 * нужно сформировать сигнал на получение данных от репозитория.
 * Обработку же запрошенных данных нужно произвести в классе, реализующем 
 * интерфес {@link ModuleInitializeHandler}. 
 * 
 * @author Vitaly Masterov
 * @since 0.1
 * @see ModuleInitializeHandler
 * @see SignalFactory 
 */
public interface ModuleInitializeEntryPoint {
		
	void entryPoint(ControlSignal signal, ControlSignalSender signalSender);

}