package network.elekmonar.signal.spec;

import java.util.UUID;

public interface ProgressSignal {
	
	public UUID getProcess();

	public void setProcess(UUID processId);
	
	public Integer getValue();
	
	public void setValue(Integer value);
	
}