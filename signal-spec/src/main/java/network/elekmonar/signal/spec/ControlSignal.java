package network.elekmonar.signal.spec;

import java.util.List;
import java.util.UUID;

/**
 * Управляющий сигнал.
 * Этот интерфейс предназначен для описания команды,
 * инициирующей выполнения какого-либо задания.
 * Маршрутизацию сигнала на нужный узел/объект автоматизации,
 * где должно выполниться задание производит модуль {@literal ontospace-signal}.
 * 
 * @author Vitaly Masterov
 * @since 0.1
 *
 */
public interface ControlSignal {
	
	/**
	 * Возвращает признак необходимости сообщать статус
	 * о процессе выполнения задачи по данному управляющему сигналу.
	 * По умолчанию, модуль отвечающий за выполнение задачи должен
	 * посылать статус, прогресс, результат или ошибку. Если же
	 * данный признак установлен в значение {@value false}, то
	 * посылать статусы в обратную сторону не нужно (в тои числе
	 * статус о завершении задачи). В таком случае сигнальный модуль
	 * будет игнорировать приём обратных сообщений, содержащих статусы
	 * и результаты. 
	 * 
	 * @return Булиновское значение признака о необходимости сообщать статус процесса
	 */
	public Boolean getReportStatus();
	
	/**
	 * Является ли модуль, сформировавший управляющий сигнал,
	 * модулем UI, отвечающим за работу пользовательского интерфейса.
	 */
	public Boolean getSourceUI();		

	/**
	 * Устанавливает признак необходимости сообщать статус процесса
	 * 
	 * @param reportStatus
	 */
	public void setReportStatus(Boolean reportStatus);
	
	/**
	 * Возвращает идентификатор узла автоматизации - источника 
	 * формирования задания.
	 * 
	 * @return Идентификатор узла автоматизации (в составе серверной группировки
	 * и находящемся на определённом объекте автоматизации)  
	 */
	public UUID getNode();
	
	public void setNode(UUID node);
	
	public String getApplication();

	public void setApplication(String application);
	
	/**
	 * @return Идентификатор модуля-источника, где был
	 * изначально сформирован сигнал
	 * 
	 */
	public String getSource();
	
	/**
	 * Задать идентификатор модуля-источника
	 * управляющего сигнала
	 * 
	 * @param source идентификатор модуля-источника
	 */
	public void setSource(String source);
	
	/**
	 * @return Идентификатор пользователя, который
	 * сформировал заявку (в случае если инициатором
	 * управляющего сигнала был пользователь, а не процесс) 
	 */
	public UUID getUser();
	
	public void setUser(UUID user);
	
	/**
	 * @return Идентификатор задачи из реестра
	 * описанных задач всех модулей
	 */
	public String getTask();
	
	public void setTask(String task);
	
	/**
	 * Дополнительные параметры, необходмиые для передачи
	 * в качестве аргументов запускаемому процессу. 
	 * @param parameterClass
	 * @param key
	 * @return
	 */
	public <T> T getParameter(Class<T> parameterClass, String key);

	/**
	 * Задать параметр для управляющего сигнала
	 *  
	 * @param key ключ параметра
	 * @param value значение параметра
	 */
	public void setParameter(String key, Object value);

	/**
	 * Получить список всех параметров, ассоциированных
	 * с данным управляющим сигналом
	 * 
	 * @return Список параметров {@link SignalParameter}
	 */
	public List<SignalParameter> getParameters();

}