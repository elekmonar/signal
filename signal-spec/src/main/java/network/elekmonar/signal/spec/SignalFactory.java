package network.elekmonar.signal.spec;

public interface SignalFactory {
	
	public ControlSignal controlSignal();
	
	public ErrorSignal errorSignal();
	
	public StateSignal stateSignal();
	
	public ProgressSignal progressSignal();

}