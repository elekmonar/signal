package network.elekmonar.signal.spec.sender;

import javax.ejb.Local;

import network.elekmonar.signal.spec.ControlSignal;

/**
 * Интерфейс, задающий сигнатуру метода посылки
 * управляющего сигнала в сторону {@literal ontospace-signal}
 * 
 * @author Vitaly Masterov
 * @since 0.1
 *
 */
@Local
public interface ControlSignalSender {
	
	public void send(ControlSignal signal);

}