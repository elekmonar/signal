package network.elekmonar.signal.persistence.test;

import static network.elekmonar.arquillian.utils.ShrinkWrapper.getDependency;
import static network.elekmonar.signal.testing.utils.SignalModules.SIGNAL_TESTING;
import static network.elekmonar.signal.testing.utils.SignalModules.SIGNAL_APP;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.testng.Arquillian;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.testng.annotations.Test;
import org.wildfly.common.Assert;

import network.elekmonar.arquillian.utils.ShrinkWrapper;

public class DbConnectionTester extends Arquillian {
	
	@Test
	public void testConnection() {
		Assert.assertTrue(true);
	}
	
	@Deployment
	public static WebArchive createDeployment() {
    	return ShrinkWrapper
    			.createDeploymentWithPersistence(SIGNAL_APP)    			
    			.addAsLibrary(getDependency(SIGNAL_TESTING));    					
	}

}