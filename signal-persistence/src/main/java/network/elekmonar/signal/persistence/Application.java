package network.elekmonar.signal.persistence;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Реестр зарегистрированных приложений
 * 
 * @author Vitaly Masterov
 * @since 0.1
 *
 */
@Entity(name = "Application")
@Table(name = "application")
public class Application implements Serializable {

	private static final long serialVersionUID = 8972126435040129296L;
	
	private Long id;
	
	private String qualifier;
	
	private String name;
	
	private String description;

	@Id
	@Column(name = "id", nullable = false)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(name = "qualifier", length = 64, nullable = false, unique = true)
	public String getQualifier() {
		return qualifier;
	}

	public void setQualifier(String qualifier) {
		this.qualifier = qualifier;
	}

	@Column(name = "name", length = 128, nullable = false)
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "description", length = 256, nullable = false)
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Override
    public boolean equals(Object value) {
        if (value == this) {
            return true;
        }
        if (!(value instanceof Application)) {
            return false;
        }
        Application obj = (Application) value;

        if (getId() == null || obj.getId() == null) {
        	return false;
        }        
        
        return obj.getId().equals(getId());
    }

	@Override
    public int hashCode() {
        int result = 29;
        result += 29 * (getId() != null ? getId().hashCode() : 0);
        return result;
    }
	
}