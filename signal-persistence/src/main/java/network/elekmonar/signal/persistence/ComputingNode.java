package network.elekmonar.signal.persistence;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Id;

/**
 * Вычислительный узел (компьютер, сервер, телефон, планшет и прочее IoT устройство),
 * на котором должна выполниться задача.
 * 
 * @author Vitaly Masterov
 * @since 0.1
 *
 */
public class ComputingNode implements Serializable {

	private static final long serialVersionUID = 4963974271727602248L;
	
	private Long id;
	
	private String name;
	
	private String description;

	@Id
	@Column(name = "id", nullable = false)	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(name = "name", length = 128, nullable = false)
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "description", length = 256, nullable = false)
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Override
    public boolean equals(Object value) {
        if (value == this) {
            return true;
        }
        if (!(value instanceof ComputingNode)) {
            return false;
        }
        ComputingNode obj = (ComputingNode) value;

        if (getId() == null || obj.getId() == null) {
        	return false;
        }        
        
        return obj.getId().equals(getId());
    }

	@Override
    public int hashCode() {
        int result = 29;
        result += 29 * (getId() != null ? getId().hashCode() : 0);
        return result;
    }
	
}