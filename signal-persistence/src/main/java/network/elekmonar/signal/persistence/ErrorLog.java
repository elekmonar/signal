package network.elekmonar.signal.persistence;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity(name = "ErrorLog")
@Table(name = "error_log")
public class ErrorLog implements Serializable {

	private static final long serialVersionUID = -5569084290167254001L;
	
	/**
	 * Идентификатор
	 */
	private Long id;
	
	/**
	 * Дата и время возникновения ошибки
	 */
	private LocalDateTime dateTime;
	
	/**
	 * Идентификатор процесса
	 */
	private UUID processId;
	
	/**
	 * Приложение
	 */
	private Application application;
	
	/**
	 * Задача
	 */
	private Task task;
	
	/**
	 * Подробный текстовый log
	 */
	private String stackTrace;

	@Id
	@Column(name = "id", nullable = false)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(name = "date_time", nullable = false)
	public LocalDateTime getDateTime() {
		return dateTime;
	}

	public void setDateTime(LocalDateTime dateTime) {
		this.dateTime = dateTime;
	}

	@Column(name = "process_id", nullable = false)
	public UUID getProcessId() {
		return processId;
	}

	public void setProcessId(UUID processId) {
		this.processId = processId;
	}

	@ManyToOne(fetch = FetchType.LAZY, optional = false)
	@JoinColumn(name = "application_id", nullable = false)
	public Application getApplication() {
		return application;
	}

	public void setApplication(Application application) {
		this.application = application;
	}

	@ManyToOne(fetch = FetchType.LAZY, optional = false)
	@JoinColumn(name = "task_id", nullable = false)
	public Task getTask() {
		return task;
	}

	public void setTask(Task task) {
		this.task = task;
	}

	@Column(name = "stack_trace", nullable = false, length = 4096)
	public String getStackTrace() {
		return stackTrace;
	}

	public void setStackTrace(String stackTrace) {
		this.stackTrace = stackTrace;
	}

	@Override
    public boolean equals(Object value) {
        if (value == this) {
            return true;
        }
        if (!(value instanceof ErrorLog)) {
            return false;
        }
        ErrorLog obj = (ErrorLog) value;

        if (getId() == null || obj.getId() == null) {
        	return false;
        }        
        
        return obj.getId().equals(getId());
    }

	@Override
    public int hashCode() {
        int result = 29;
        result += 29 * (getId() != null ? getId().hashCode() : 0);
        return result;
    }
	
}