package network.elekmonar.signal.service.impl;

import java.io.Serializable;

import javax.ejb.Stateless;
import javax.ws.rs.core.Response;

import network.elekmonar.signal.service.SignalInfoService;

@Stateless
public class SignalInfoServiceImpl implements SignalInfoService, Serializable {

	private static final long serialVersionUID = -1097605079518603729L;

	@Override
	public Response getInfo() {
		System.out.println("Hello from Signal Module ...");
		return Response.noContent().build();
	}

}