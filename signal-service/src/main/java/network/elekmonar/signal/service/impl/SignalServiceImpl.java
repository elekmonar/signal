package network.elekmonar.signal.service.impl;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.UUID;

import javax.annotation.PostConstruct;
import javax.ejb.Stateless;
import javax.enterprise.event.Event;
import javax.inject.Inject;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;

import org.apache.commons.io.IOUtils;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import network.elekmonar.signal.beans.ControlSignalBean;
import network.elekmonar.signal.service.SignalService;
import network.elekmonar.signal.service.repr.ProcIdRepr;
import network.elekmonar.signal.spec.SignalFactory;
import network.elekmonar.signal.spec.events.ControlSignalEvent;

@Stateless
public class SignalServiceImpl implements SignalService, Serializable {

	private static final long serialVersionUID = 4184542340049447451L;
	
	@Inject
	private SignalFactory signalFactory;
	
	@Inject
	private Event<ControlSignalEvent> csEvent;
	
	private ObjectMapper mapper;
	
	@PostConstruct
	private void doInit() {
		mapper = new ObjectMapper();
	}

	@Override
	public Response run(String qualifier, String application, UUID node, InputStream is) {
		ControlSignalBean controlSignal = (ControlSignalBean)signalFactory.controlSignal();
		controlSignal.setTask(qualifier);
		controlSignal.setApplication(application);
		csEvent.fire(new ControlSignalEvent(controlSignal));
		parseParameters(is, controlSignal);
		ProcIdRepr repr = new ProcIdRepr(controlSignal.getProcess());
		
		return Response
				.ok(repr)
				.build();
	}
	
	private void parseParameters(InputStream is, ControlSignalBean controlSignal) {
		if (is == null) {
			return;
		}
		
		InputStream jsonIs;
		try {
			byte[] jsonData = IOUtils.toByteArray(is);
			if (jsonData.length == 0) {
				return;
			}
			jsonIs = new ByteArrayInputStream(jsonData);	
		} catch(IOException e) {
			throw new WebApplicationException(e);
		}		
		
		try {
			TypeReference<HashMap<String, String>> typeRef = new TypeReference<HashMap<String, String>>() {};
			Map<String, String> params = mapper.readValue(jsonIs, typeRef);
			for (Entry<String, String> entry : params.entrySet()) {				
				controlSignal.setParameter(entry.getKey(), entry.getValue());
			}
		} catch (IOException e) {
			throw new WebApplicationException(e);
		}
	}

	@Override
	public Response hello() {
		return Response.ok("Hello ...").build();
	}

}