package network.elekmonar.signal.service;

import java.io.InputStream;
import java.util.UUID;

import javax.ejb.Local;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Local
@Path("/tasks")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public interface SignalService {

	@GET
	@Path("/")
	Response hello();	
	
	/** 
	 * Сформировать сигнал для запуска зазачи на выполнение
	 * 
	 * @param qualifier Квалификатор задачи
	 * @param is Payload
	 * @param app Квалификатор приложения, которое должно выполнить задачу
	 * @param node Идентификатор узла, на который необходимо доставить сигнал 
	 * @return Идентификатор, присвоенный процессу
	 */
	@POST
	@Path("/{qualifier}")
	Response run(
			@PathParam("qualifier") String qualifier, 
			@QueryParam("app") String application, 
			@QueryParam("node") UUID node, 
			InputStream is);

}