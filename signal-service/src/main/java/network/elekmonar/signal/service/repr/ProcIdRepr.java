package network.elekmonar.signal.service.repr;

import java.io.Serializable;
import java.util.UUID;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ProcIdRepr implements Serializable {

	private static final long serialVersionUID = 5797351609127653145L;
	
	private UUID id;
	
	public ProcIdRepr(UUID id) {
		this.id = id;
	}	

	@JsonProperty(value = "id")
	public UUID getId() {
		return id;
	}

	public void setId(UUID id) {
		this.id = id;
	}

}