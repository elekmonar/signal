package network.elekmonar.signal.service.test;

import static network.elekmonar.arquillian.utils.ShrinkWrapper.getDependency;
import static network.elekmonar.signal.testing.utils.SignalModules.SIGNAL_TESTING;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.testng.Arquillian;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.testng.Assert;
import org.testng.annotations.Test;

import network.elekmonar.arquillian.utils.ShrinkWrapper;


public class SignalInfoServiceTester extends Arquillian {
	
	@Test
	public void testService() {
		System.out.println("call test service ...");
		Assert.assertTrue(true);
	}	

    @Deployment
    public static WebArchive createDeployment() {
    	return ShrinkWrapper
    			.createDeployment()    			
    			.addAsLibrary(getDependency(SIGNAL_TESTING));    			
    }
	
}