package network.elekmonar.signal.testing.utils;

import network.elekmonar.arquillian.utils.spi.AppTestingConfig;
import network.elekmonar.arquillian.utils.spi.AppTestingConfigReader;

public class SignalAppTestingConfigReader implements AppTestingConfigReader {

	@Override
	public Class<? extends AppTestingConfig> configClass() {
		return SignalAppTestingConfig.class;
	}

}