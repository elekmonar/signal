package network.elekmonar.signal.testing.utils;

import network.elekmonar.arquillian.utils.spi.AppTestingConfig;

public class SignalAppTestingConfig implements AppTestingConfig {

	@Override
	public String groupId() {
		return "network.elekmonar.signal";
	}

	@Override
	public String version() {
		return "0.1-SNAPSHOT";
	}

	@Override
	public Class<SignalModules> modules() {
		return SignalModules.class;
	}

}