package network.elekmonar.signal.testing.utils;

import network.elekmonar.arquillian.utils.spi.Modules;

public class SignalModules implements Modules {
	
	public static final String SIGNAL_TESTING = "signal-testing";
	
	public static final String SIGNAL_SPEC = "signal-spec";
	
	public static final String SIGNAL_BEANS = "signal-beans";
	
	public static final String SIGNAL_CONFIG = "signal-config";
	
	public static final String SIGNAL_DISPATCHER = "signal-dispatcher";
	
	public static final String SIGNAL_EMBEDDABLE = "signal-embeddable";
	
	public static final String SIGNAL_IMPL = "signal-impl";
	
	public static final String SIGNAL_PERSISTENCE = "signal-persistence";
	
	public static final String SIGNAL_SERVICE = "signal-service";
	
	public static final String SIGNAL_WAR = "signal-war";
	
	public static final String SIGNAL_APP = "signal-app";

}