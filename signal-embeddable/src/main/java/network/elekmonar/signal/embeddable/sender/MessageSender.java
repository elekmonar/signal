package network.elekmonar.signal.embeddable.sender;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.jms.JMSException;
import javax.jms.MessageProducer;
import javax.jms.Queue;
import javax.jms.QueueConnection;
import javax.jms.QueueConnectionFactory;
import javax.jms.QueueSession;
import javax.jms.Session;

public abstract class MessageSender implements Serializable {

	private static final long serialVersionUID = -7719050313702567720L;
	
	protected QueueConnection connection;
    
    protected QueueSession session;
    
    protected MessageProducer producer;
    
    protected abstract QueueConnectionFactory getConnectionFactory();
    
    protected abstract Queue getQueue();
    
    @PostConstruct
    protected void init() {
        try {
            connection = getConnectionFactory().createQueueConnection();
            connection.start();            
            session = connection.createQueueSession(false, Session.AUTO_ACKNOWLEDGE);
            producer = session.createProducer(getQueue());
        } catch (JMSException e) {
            e.printStackTrace();
        }
    }    
    
    @PreDestroy
    protected void close() {
        if (producer != null) {
            try {
                producer.close();
            } catch (JMSException e) {
                e.printStackTrace();
            }
        }
        if (session != null) {
            try {
                session.close();
            } catch (JMSException e) {
                e.printStackTrace();
            }
        }
        if (connection != null) {
            try {
                connection.close();
            } catch (JMSException e) {
                e.printStackTrace();
            }
        }
    }

}