package network.elekmonar.signal.embeddable.listener;

import java.io.Serializable;
import java.util.UUID;

import javax.ejb.ActivationConfigProperty;
import javax.ejb.EJB;
import javax.ejb.MessageDriven;
import javax.enterprise.inject.UnsatisfiedResolutionException;
import javax.enterprise.inject.spi.CDI;
import javax.inject.Inject;
import javax.jms.Message;
import javax.jms.MessageListener;

import network.elekmonar.signal.spec.sender.ControlSignalSender;
import network.elekmonar.signal.spec.spi.ProcessSignalContext;

/*
import ru.ontospace.signal.api.factory.SignalFactory;
import ru.ontospace.signal.api.factory.ProcessSignalContext;
import ru.ontospace.signal.api.sender.ControlSignalSender;
import ru.ontospace.signal.beans.ControlSignalBean;
import ru.ontospace.signal.module.context.ProcessContextStorage;
import ru.ontospace.signal.module.context.ProcessSignalContextImpl;
import ru.ontospace.signal.spi.ModuleInitializeEntryPoint;
import ru.ontospace.signal.spi.ProcessCallbackHandler;
import ru.ontospace.signal.spi.annotations.ProcessCallback;
*/

/**
 * Слушатель служебных JMS-сообщений, рассылаемых как {@literal topic}
 * сигнальным модулем. Одним из важных таких сообщений является сообщение 
 * о необходимости выпонить процедуру инициализации заданного модуля.
 * 
 * @author Vitaly Masterov
 * @since 0.1
 * @see SignalFactory
 * @see ModuleInitializeEntryPoint
 *
 */
@MessageDriven(
		name = "ServiceSignalTopicListener",
		activationConfig = {
				@ActivationConfigProperty(propertyName = "destination", propertyValue = "java:/jms/topic/app"),
				@ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Topic"),
                @ActivationConfigProperty(propertyName = "acknowledgeMode", propertyValue = "AUTO_ACKNOWLEDGE")
		}
)
public class ServiceSignalTopicListener implements MessageListener, Serializable {

	private static final long serialVersionUID = -984207315301401727L;
	
	@EJB
	private ControlSignalSender signalSender;
	
	@Inject
	private ProcessSignalContext processContext;
	
	@Override
	public void onMessage(Message message) {
	}

}