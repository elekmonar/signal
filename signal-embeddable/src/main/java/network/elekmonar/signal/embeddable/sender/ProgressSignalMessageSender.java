package network.elekmonar.signal.embeddable.sender;

import static network.elekmonar.signal.spec.enums.SignalType.PROGRESS;
import static network.elekmonar.signal.spec.enums.SignalType.SIGNAL_TYPE;

import java.io.Serializable;

import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.jms.JMSException;
import javax.jms.ObjectMessage;
import javax.jms.Queue;
import javax.jms.QueueConnectionFactory;

import network.elekmonar.signal.beans.ProgressSignalBean;
import network.elekmonar.signal.spec.ProgressSignal;

@Stateless
public class ProgressSignalMessageSender extends MessageSender implements Serializable {

	private static final long serialVersionUID = -237319155419579261L;	

	@Resource(mappedName = "java:/jms/factory/self")
	private QueueConnectionFactory connectionFactory;

	@Resource(mappedName = "java:/jms/queue/signal/progress")
	private Queue queue;

	@Override
	protected QueueConnectionFactory getConnectionFactory() {
		return connectionFactory;
	}

	@Override
	protected Queue getQueue() {
		return queue;
	}
	
	public void send(ProgressSignal signal) {
        ObjectMessage objMessage;
        try {        	
            objMessage = session.createObjectMessage();
            objMessage.setStringProperty(SIGNAL_TYPE, PROGRESS.name());
            objMessage.setObject((ProgressSignalBean)signal);
            producer.send(objMessage);
        } catch (JMSException e) {
            e.printStackTrace();
        }		
	}	
	
}