package network.elekmonar.signal.embeddable.sender;

import static network.elekmonar.signal.spec.enums.SignalType.SIGNAL_TYPE;
import static network.elekmonar.signal.spec.enums.SignalType.STATE;

import java.io.Serializable;

import javax.annotation.PreDestroy;
import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.jms.JMSException;
import javax.jms.ObjectMessage;
import javax.jms.Queue;
import javax.jms.QueueConnectionFactory;

import network.elekmonar.signal.beans.StateSignalBean;
import network.elekmonar.signal.spec.StateSignal;

@Stateless
public class StateSignalMessageSender extends MessageSender implements Serializable {

	private static final long serialVersionUID = -5801868258213682207L;	

	@Resource(mappedName = "java:/jms/factory/self")
	private QueueConnectionFactory connectionFactory;

	@Resource(mappedName = "java:/jms/queue/signal/state")
	private Queue queue;

	@Override
	protected QueueConnectionFactory getConnectionFactory() {
		return connectionFactory;
	}

	@Override
	protected Queue getQueue() {
		return queue;
	}
	
	public void send(StateSignal signal) {
        ObjectMessage objMessage;
        try {
            objMessage = session.createObjectMessage();
            objMessage.setStringProperty(SIGNAL_TYPE, STATE.name());
            objMessage.setObject((StateSignalBean)signal);
            producer.send(objMessage);
        } catch (JMSException e) {
            e.printStackTrace();
        }		
	}	
	
}