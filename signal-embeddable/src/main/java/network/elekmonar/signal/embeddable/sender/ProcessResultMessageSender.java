package network.elekmonar.signal.embeddable.sender;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.UUID;

import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.jms.BytesMessage;
import javax.jms.JMSException;
import javax.jms.Queue;
import javax.jms.QueueConnectionFactory;

@Stateless
public class ProcessResultMessageSender extends MessageSender implements Serializable {

	private static final long serialVersionUID = -4443857019300535156L;
	
	private static final String PROCESS_ID = "process";
	
	private static final String CONTENT_LENGTH = "CONTENT_LENGTH";
	
	@Resource(mappedName = "java:/jms/factory/self")
	private QueueConnectionFactory connectionFactory;

	@Resource(mappedName = "java:/jms/queue/signal/result")
	private Queue queue;

	@Override
	protected QueueConnectionFactory getConnectionFactory() {
		return connectionFactory;
	}

	@Override
	protected Queue getQueue() {
		return queue;
	}
	
	public void send(UUID processId, Serializable result) {
		BytesMessage bytesMessage;
        try {
            bytesMessage = session.createBytesMessage();
            bytesMessage.setStringProperty(PROCESS_ID, processId.toString());
            
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            ObjectOutputStream os = new ObjectOutputStream(out);
            os.writeObject(result);
            byte[] byteArrayData = out.toByteArray();
            bytesMessage.writeBytes(byteArrayData);
            bytesMessage.setIntProperty(CONTENT_LENGTH, byteArrayData.length);
                        
            producer.send(bytesMessage);
        } catch (JMSException e) {
            e.printStackTrace();
        } catch(IOException e) {
        	e.printStackTrace();
        }		
	}	

}