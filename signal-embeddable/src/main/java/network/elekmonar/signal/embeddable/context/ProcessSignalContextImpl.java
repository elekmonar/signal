package network.elekmonar.signal.embeddable.context;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.io.Serializable;
import java.util.UUID;

import javax.ejb.EJB;
import javax.enterprise.event.Event;
import javax.inject.Inject;

import network.elekmonar.signal.beans.ErrorSignalBean;
import network.elekmonar.signal.beans.ProgressSignalBean;
import network.elekmonar.signal.beans.StateSignalBean;
import network.elekmonar.signal.embeddable.sender.ErrorSignalMessageSender;
import network.elekmonar.signal.embeddable.sender.ProcessResultMessageSender;
import network.elekmonar.signal.embeddable.sender.ProgressSignalMessageSender;
import network.elekmonar.signal.embeddable.sender.StateSignalMessageSender;
import network.elekmonar.signal.spec.ControlSignal;
import network.elekmonar.signal.spec.enums.ProcessState;
import network.elekmonar.signal.spec.events.ControlSignalEvent;
import network.elekmonar.signal.spec.spi.ProcessCallbackHandler;
import network.elekmonar.signal.spec.spi.ProcessSignalContext;

public class ProcessSignalContextImpl implements ProcessSignalContext, Serializable {

	private static final long serialVersionUID = -3726631040769602239L;
			
	@EJB
	private ErrorSignalMessageSender errorSignalMessageSender;

	@EJB
	private StateSignalMessageSender stateSignalMessageSender;

	@EJB
	private ProgressSignalMessageSender progressSignalMessageSender;
	
	@EJB
	private ProcessResultMessageSender processResultMessageSender;
		
	@Inject
	private Event<ControlSignalEvent> controlSignalEvent;	
	
	private UUID processId;	
	
	private ControlSignal controlSignal;
	
	/**
	 * Ассоциированный с данным контекстом класс {@literal callback handler}.
	 */
	private Class<? extends ProcessCallbackHandler<?>> callbackHandlerClass;

	/**
	 * То же самое, что и {@literal callbackHandler},
	 * только применяется в случае задействования
	 * группы запросов 
	 */
	private Class<? extends ProcessCallbackHandler<?>> callbackHandlerClasses;
	
	@Override
	public UUID getProcessId() {
		return processId;
	}

	public void setProcessId(UUID processId) {
		this.processId = processId;
	}
		
	@Override
	public ControlSignal getControlSignal() {
		return controlSignal;
	}

	public void setControlSignal(ControlSignal controlSignal) {
		this.controlSignal = controlSignal;
	}	

	@Override
	public void sendControlSignal(ControlSignal signal) {
		controlSignalEvent.fire(new ControlSignalEvent(signal));
	}
	
	@Override
	public void sendError(String code) {
		ErrorSignalBean signal = new ErrorSignalBean(processId, code);
		errorSignalMessageSender.send(signal);
	}

	@Override
	public void sendError(String code, Exception e) {
		try (
				ByteArrayOutputStream out = new ByteArrayOutputStream();
				PrintStream stream = new PrintStream(out);
		) {
			e.printStackTrace(stream);		
			ErrorSignalBean signal = new ErrorSignalBean(processId, code, out.toByteArray());		
			errorSignalMessageSender.send(signal);			
		} catch(IOException ioe) {
			ioe.printStackTrace();
		}
	}
	
	@Override
	public void sendState(ProcessState state) {
		StateSignalBean signal = new StateSignalBean(processId, state);
		stateSignalMessageSender.send(signal);
	}

	@Override
	public void sendDone() {
		StateSignalBean signal = new StateSignalBean(processId, ProcessState.DONE);
		stateSignalMessageSender.send(signal);
	}
	
	@Override
	public void sendProgress(Integer value) {
		ProgressSignalBean signal = new ProgressSignalBean(processId, value);
		progressSignalMessageSender.send(signal);
	}

	@Override
	public void sendResult(Serializable result) {
		processResultMessageSender.send(processId, result);
	}	
			
	public Class<? extends ProcessCallbackHandler<?>> getCallbackHandlerClass() {
		return callbackHandlerClass;
	}

	public void setCallbackHandlerClass(Class<? extends ProcessCallbackHandler<?>> callbackHandlerClass) {
		this.callbackHandlerClass = callbackHandlerClass;
	}

	public Class<? extends ProcessCallbackHandler<?>> getCallbackHandlerClasses() {
		return callbackHandlerClasses;
	}

	public void setCallbackHandlerClasses(Class<? extends ProcessCallbackHandler<?>> callbackHandlerClasses) {
		this.callbackHandlerClasses = callbackHandlerClasses;
	}
	
}