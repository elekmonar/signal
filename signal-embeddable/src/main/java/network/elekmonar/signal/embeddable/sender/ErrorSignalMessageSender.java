package network.elekmonar.signal.embeddable.sender;

import static network.elekmonar.signal.spec.enums.SignalType.ERROR;
import static network.elekmonar.signal.spec.enums.SignalType.SIGNAL_TYPE;

import java.io.Serializable;

import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.jms.JMSException;
import javax.jms.ObjectMessage;
import javax.jms.Queue;
import javax.jms.QueueConnectionFactory;

import network.elekmonar.signal.beans.ErrorSignalBean;
import network.elekmonar.signal.spec.ErrorSignal;

@Stateless
public class ErrorSignalMessageSender extends MessageSender implements Serializable {

	private static final long serialVersionUID = 8582016039145710348L;

	@Resource(mappedName = "java:/jms/factory/self")
	private QueueConnectionFactory connectionFactory;

	@Resource(mappedName = "java:/jms/queue/signal/error")
	private Queue queue;

	@Override
	protected QueueConnectionFactory getConnectionFactory() {
		return connectionFactory;
	}

	@Override
	protected Queue getQueue() {
		return queue;
	}
	
	public void send(ErrorSignal signal) {
        ObjectMessage objMessage;
        try {
            objMessage = session.createObjectMessage();
            objMessage.setStringProperty(SIGNAL_TYPE, ERROR.name());
            objMessage.setObject((ErrorSignalBean)signal);
            producer.send(objMessage);
        } catch (JMSException e) {
            e.printStackTrace();
        }		
	}

}