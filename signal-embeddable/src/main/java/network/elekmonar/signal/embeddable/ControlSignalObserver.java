package network.elekmonar.signal.embeddable;

import java.io.Serializable;

import javax.ejb.EJB;
import javax.enterprise.event.Observes;

import network.elekmonar.signal.spec.ControlSignal;
import network.elekmonar.signal.spec.events.ControlSignalEvent;
import network.elekmonar.signal.spec.sender.ControlSignalSender;

/**
 * Отлавливатель события {@link ControlSignalEvent},
 * который с помощью EJB-JMS-посыльщика отправляет
 * сообщение адресату.
 * 
 * @author Vitaly Masterov
 * @since 0.1
 * @see ControlSignalEvent
 * @see ControlSignal
 * @see ControlSignalSender
 *
 */
public class ControlSignalObserver implements Serializable {

	private static final long serialVersionUID = 8367677669257943286L;
	
	@EJB
	private ControlSignalSender controlSignalSender;	
	
	public void observesControlSignal(@Observes ControlSignalEvent event) {
		controlSignalSender.send(event.getSignal());
	}

}