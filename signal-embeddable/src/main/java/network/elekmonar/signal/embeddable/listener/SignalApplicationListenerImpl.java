package network.elekmonar.signal.embeddable.listener;

import java.io.Serializable;
import java.util.Map;
import java.util.UUID;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.enterprise.inject.Any;
import javax.enterprise.inject.Instance;
import javax.enterprise.inject.UnsatisfiedResolutionException;
import javax.enterprise.inject.spi.CDI;
import javax.inject.Inject;
import javax.inject.Named;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.ObjectMessage;

import org.apache.deltaspike.core.util.metadata.AnnotationInstanceProvider;
import static network.elekmonar.signal.spec.enums.ProcessState.DONE;
import static network.elekmonar.signal.spec.enums.SignalType.SIGNAL_TYPE;
import static network.elekmonar.signal.spec.enums.SignalType.CONTROL;

import network.elekmonar.signal.beans.ControlSignalBean;
import network.elekmonar.signal.embeddable.context.ProcessSignalContextImpl;
import network.elekmonar.signal.spec.enums.SignalType;
import network.elekmonar.signal.spec.spi.ProcessLauncher;
import network.elekmonar.signal.spec.spi.ProcessSignalContext;
import network.elekmonar.signal.spec.spi.SignalModuleListener;

/**
 * Класс, который используется в качестве делегата
 * в MDB-слушателях управляющих сигналов {@link ControlSignal},
 * расположенных в соответствующих функциональных модулях.
 * Его задача задать контекст выполнения задачи {@link ProcessSignalContext}
 * и стартовать процесс выполнения задачи.
 * Поскольку сам делегат используется в контексте {@literal MDB}, то в случае
 * необходиомсти ему будет обеспечена транзакционность.
 * 
 * @author Vitaly Masterov
 * @since 0.1
 * @see SignalModuleListener
 * @see ProcessSignalContext
 * @see ProcessSignalContextImpl
 * @see MessageListener
 *
 */
@Stateless
public class SignalApplicationListenerImpl implements SignalModuleListener, MessageListener, Serializable {

	private static final long serialVersionUID = 5048180455272696293L;
	
	private static final String PROCESS = "process";
	
	private static final String VALUE = "value";
		              
	@Inject
	private ProcessSignalContext processContext;
	
	@Inject @Any
	private Instance<ProcessLauncher> processLaunchers;
	
	@Override
	public void onMessage(Message message) {		
		try {
			ObjectMessage objMsg = (ObjectMessage)message;			
			String signalTypeProperty = objMsg.getStringProperty(SIGNAL_TYPE);
			if (signalTypeProperty == null) {				
				System.err.println("Property message 'SignalType' is empty !");
				return;
			}
			
			SignalType signalType = SignalType.valueOf(signalTypeProperty);
			
			if (signalType.equals(CONTROL)) {
				runProcessLauncher(objMsg);
			}
		} catch (JMSException e) {
			System.out.println("=== Fucking Fuck ===");
			e.printStackTrace();
			processContext.sendError("1000");
		} catch(Error e) {			
			System.out.println("============== TASK ERROR ============== TASK ERROR ==============");
			e.printStackTrace();			
			System.out.println("============== TASK ERROR ============== TASK ERROR ==============");
			processContext.sendError("1000");
		} catch(Exception e) {
			System.out.println("+++++++++++++++++++++++++++");
			e.printStackTrace();
			System.out.println("+++++++++++++++++++++++++++");
		}
	}
	
	private void runProcessLauncher(ObjectMessage objMsg) throws JMSException {
		UUID processId = UUID.fromString(objMsg.getStringProperty(PROCESS));
		ControlSignalBean signal = (ControlSignalBean)objMsg.getObject();
		ProcessSignalContextImpl context = (ProcessSignalContextImpl)processContext;
				
		context.setProcessId(processId);		
		context.setControlSignal(signal);
		Named ann = AnnotationInstanceProvider.of(Named.class, Map.of(VALUE, signal.getTask().toString()));
		ProcessLauncher launcher;
		try {
			launcher = processLaunchers.select(ann).get();	
		} catch(UnsatisfiedResolutionException e) {
			throw e;
			// TODO
		}
		
		launcher.launch(context);
	}
		
}