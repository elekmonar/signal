package network.elekmonar.signal.embeddable.sender;

import static network.elekmonar.signal.spec.enums.SignalType.CONTROL;
import static network.elekmonar.signal.spec.enums.SignalType.SIGNAL_TYPE;

import java.io.Serializable;

import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.jms.JMSException;
import javax.jms.ObjectMessage;
import javax.jms.Queue;
import javax.jms.QueueConnectionFactory;

import network.elekmonar.signal.spec.ControlSignal;
import network.elekmonar.signal.spec.sender.ControlSignalSender;

@Stateless
public class ControlSignalSenderImpl extends MessageSender implements ControlSignalSender, Serializable {

	private static final long serialVersionUID = -4779693068212254540L;
	
	private static final String MESSAGE_SELECTOR = "messageSelector";
	
	@Resource(mappedName = "java:/jms/factory/self")
	private QueueConnectionFactory connectionFactory;

	@Resource(mappedName = "java:/jms/queue/signal/control")
	private Queue queue;

	@Override
	protected QueueConnectionFactory getConnectionFactory() {
		return connectionFactory;
	}

	@Override
	protected Queue getQueue() {
		return queue;
	}
	
	public void send(ControlSignal signal) {
        ObjectMessage objMessage;
        try {        	
            objMessage = session.createObjectMessage();
            objMessage.setStringProperty(MESSAGE_SELECTOR, signal.getApplication());
            objMessage.setStringProperty(SIGNAL_TYPE, CONTROL.name());
            objMessage.setObject((Serializable)signal);
            producer.send(objMessage);
        } catch (JMSException e) {
            e.printStackTrace();
        }		
	}	

}