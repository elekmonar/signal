package network.elekmonar.signal.embeddable;

import java.io.Serializable;
import java.util.UUID;

import network.elekmonar.signal.beans.ControlSignalBean;
import network.elekmonar.signal.beans.ErrorSignalBean;
import network.elekmonar.signal.beans.ProgressSignalBean;
import network.elekmonar.signal.beans.StateSignalBean;
import network.elekmonar.signal.spec.ControlSignal;
import network.elekmonar.signal.spec.ErrorSignal;
import network.elekmonar.signal.spec.ProgressSignal;
import network.elekmonar.signal.spec.SignalFactory;
import network.elekmonar.signal.spec.StateSignal;

public class SignalFactoryEmbedImpl implements SignalFactory, Serializable {

	private static final long serialVersionUID = -4729184674530007747L;
		
	@Override
	public ControlSignal controlSignal() {
		ControlSignalBean signal = new ControlSignalBean();
		signal.setProcess(UUID.randomUUID());
		return signal;
	}

	@Override
	public ErrorSignal errorSignal() {
		ErrorSignalBean signal = new ErrorSignalBean();
		
		return signal;
	}

	@Override
	public StateSignal stateSignal() {
		StateSignalBean signal = new StateSignalBean();
		
		return signal;
	}

	@Override
	public ProgressSignal progressSignal() {
		ProgressSignal signal = new ProgressSignalBean();
		
		return signal;
	}
	
}
