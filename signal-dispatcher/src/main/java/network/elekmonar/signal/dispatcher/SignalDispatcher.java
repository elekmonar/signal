package network.elekmonar.signal.dispatcher;

import java.io.Serializable;

import javax.ejb.Asynchronous;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.enterprise.event.Observes;

import network.elekmonar.signal.beans.ControlSignalBean;
import network.elekmonar.signal.dispatcher.sender.ToAppQueueSender;
import network.elekmonar.signal.spec.events.ControlSignalEvent;

@Stateless
public class SignalDispatcher implements Serializable {

	private static final long serialVersionUID = -5323731815271623981L;
	
	@EJB
	private ToAppQueueSender queueSender;
	
	@Asynchronous
	public void dispatch(@Observes ControlSignalEvent event) {
		ControlSignalBean signal = (ControlSignalBean)event.getSignal();
		queueSender.send((Serializable)event.getSignal(), signal.getApplication(), signal.getProcess());
	}

}