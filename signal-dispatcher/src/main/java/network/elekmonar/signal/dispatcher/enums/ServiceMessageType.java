package network.elekmonar.signal.dispatcher.enums;

/**
 * Перечисление, задающее тип служебного
 * сообщения, посылаемого модулю.
 * 
 * @author Vitaly Masterov
 * @since 0.4
 *
 */
public enum ServiceMessageType {
	
	/**
	 * Провести инициализацию
	 */
	INITIALIZATION,
	
	/**
	 * Запросить статсу модуля
	 */
	PING,
	
	/**
	 * Прервать процесс
	 */
	INTERRUPT,
	
	/**
	 * Завершить работу модуля
	 * и выгрузить его из контейнера
	 * (произвести undeploy)
	 */
	SHUTDOWN;

}