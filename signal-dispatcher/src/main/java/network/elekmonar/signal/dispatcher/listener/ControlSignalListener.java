package network.elekmonar.signal.dispatcher.listener;

import java.io.Serializable;

import javax.ejb.ActivationConfigProperty;
import javax.ejb.EJB;
import javax.ejb.MessageDriven;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.ObjectMessage;

import network.elekmonar.signal.beans.ControlSignalBean;
import network.elekmonar.signal.dispatcher.sender.ToAppQueueSender;

@MessageDriven(name = "ControlSignalListener", activationConfig = {
		@ActivationConfigProperty(propertyName = "destination", propertyValue = "java:/jms/queue/signal/control"),
		@ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Queue"),
		@ActivationConfigProperty(propertyName = "acknowledgeMode", propertyValue = "AUTO_ACKNOWLEDGE") 
})
public class ControlSignalListener implements MessageListener, Serializable {

	private static final long serialVersionUID = 5026271452167770047L;
	
	@EJB
	private ToAppQueueSender queueSender;
	
	@Override
	public void onMessage(Message message) {
		System.out.println("[ControlSignalListener] ...");
		ObjectMessage objMsg = (ObjectMessage)message;
		try {
			ControlSignalBean controlSignal = (ControlSignalBean)objMsg.getObject();
			queueSender.send(objMsg.getObject(), controlSignal.getApplication(), controlSignal.getProcess());
		} catch (JMSException e) {
			throw new RuntimeException(e);
		}
	}

}
