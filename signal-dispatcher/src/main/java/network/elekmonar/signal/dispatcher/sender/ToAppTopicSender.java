package network.elekmonar.signal.dispatcher.sender;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.Session;
import javax.jms.Topic;
import javax.jms.TopicConnection;
import javax.jms.TopicConnectionFactory;
import javax.jms.TopicPublisher;
import javax.jms.TopicSession;

import network.elekmonar.signal.dispatcher.enums.ServiceMessageType;

/**
 * Рассыльщик служебных JMS-сообщений от
 * сигнального модуля в сторону остальных модулей.
 * Рассылка осуществляется массово в виде топика.
 * 
 * @author Vitaly Masterov
 * @since 0.4
 * @see ServiceMessageType
 */
@Stateless
public class ToAppTopicSender implements Serializable {

	private static final long serialVersionUID = -2125435655981046372L;
	
	private static final String MESSAGE_TYPE = "MESSAGE_TYPE";
	
	@Resource(mappedName = "java:/jms/factory/self")
	private TopicConnectionFactory connectionFactory;

	@Resource(mappedName = "java:/jms/topic/app")
	private Topic topic;
	
	private TopicConnection connection;
	
	private TopicSession session;
	
	private TopicPublisher publisher;
	
    @PostConstruct
    protected void init() {
        try {
            connection = connectionFactory.createTopicConnection();
            connection.start();            
            session = connection.createTopicSession(false, Session.AUTO_ACKNOWLEDGE);
            publisher = session.createPublisher(topic);
        } catch (JMSException e) {
            e.printStackTrace();
        }
    }    
	
    @PreDestroy
    protected void close() {
        if (publisher != null) {
            try {
            	publisher.close();
            } catch (JMSException e) {
                e.printStackTrace();
            }
        }
        if (session != null) {
            try {
                session.close();
            } catch (JMSException e) {
                e.printStackTrace();
            }
        }
        if (connection != null) {
            try {
                connection.close();
            } catch (JMSException e) {
                e.printStackTrace();
            }
        }
    }

    public void send(ServiceMessageType messageType) {
    	try {
    		Message message = session.createMessage();
    		message.setStringProperty(MESSAGE_TYPE, messageType.name());
    		publisher.publish(message);
    	} catch(JMSException e) {
    		e.printStackTrace();
    	}    	
    }
    
}