package network.elekmonar.signal.dispatcher.sender;

import java.io.Serializable;
import java.util.UUID;

import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.jms.JMSException;
import javax.jms.ObjectMessage;
import javax.jms.Queue;
import javax.jms.QueueConnectionFactory;

import static network.elekmonar.signal.spec.enums.SignalType.SIGNAL_TYPE;
import static network.elekmonar.signal.spec.enums.SignalType.CONTROL;
import static network.elekmonar.signal.spec.enums.SignalType.ERROR;
import static network.elekmonar.signal.spec.enums.SignalType.PROGRESS;
import static network.elekmonar.signal.spec.enums.SignalType.STATE;

import network.elekmonar.signal.dispatcher.jms.MessageSender;
import network.elekmonar.signal.spec.ControlSignal;
import network.elekmonar.signal.spec.ErrorSignal;
import network.elekmonar.signal.spec.ProgressSignal;
import network.elekmonar.signal.spec.StateSignal;

/**
 * Посыльщик JMS-сообщений в очередь, предназначенную
 * для отправки сигналов и данных раличным модулям системы.
 * Поскольку для нескольких модулей организована одна очередь,
 * то адресат определяется дополнительным фильтрационным параметром.
 * 
 * @author Vitaly Masterov
 * @since 0.1
 *
 */
@Stateless
public class ToAppQueueSender extends MessageSender {

	private static final long serialVersionUID = 7218768934955927339L;

	/**
	 * Метка модуля, которому нужно передать сообщение
	 */
	private static final String TARGET = "target";
	
	private static final String PROCESS_ID = "process";

	@Resource(mappedName = "java:/jms/factory/self")
	private QueueConnectionFactory connectionFactory;

	@Resource(mappedName = "java:/jms/queue/app")
	private Queue queue;

	@Override
	protected QueueConnectionFactory getConnectionFactory() {
		return connectionFactory;
	}

	@Override
	protected Queue getQueue() {
		return queue;
	}
	
	public void send(Serializable msgData, String application, UUID processId) {
        ObjectMessage objMessage;
        try {
            objMessage = session.createObjectMessage();
            objMessage.setStringProperty(TARGET, application);
            objMessage.setStringProperty(PROCESS_ID, processId.toString());
            
    		if (msgData instanceof ControlSignal) {
    			objMessage.setStringProperty(SIGNAL_TYPE, CONTROL.name());
    		} else if (msgData instanceof StateSignal) {
    			objMessage.setStringProperty(SIGNAL_TYPE, STATE.name());
    		} else if (msgData instanceof ErrorSignal) {
    			objMessage.setStringProperty(SIGNAL_TYPE, ERROR.name());
    		} else if (msgData instanceof ProgressSignal) {
    			objMessage.setStringProperty(SIGNAL_TYPE, PROGRESS.name());
    		}            
            
            objMessage.setObject(msgData);
            producer.send(objMessage);
        } catch (JMSException e) {
            e.printStackTrace();
        }
	}

}