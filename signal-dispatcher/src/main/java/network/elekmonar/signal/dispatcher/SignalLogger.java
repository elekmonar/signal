package network.elekmonar.signal.dispatcher;

import static org.jboss.logging.Logger.Level.TRACE;
import static org.jboss.logging.Logger.Level.DEBUG;
import static org.jboss.logging.Logger.Level.ERROR;

import org.jboss.logging.BasicLogger;
import org.jboss.logging.Logger;
import org.jboss.logging.annotations.LogMessage;
import org.jboss.logging.annotations.Message;
import org.jboss.logging.annotations.MessageLogger;

/**
 * Интерфейс использующийся для журналирования в лог-файл
 * отладочной информации модуля {@literal ontospace-signal}.
 * 
 * @author Vitaly Masterov
 * @see 0.1
 * @see BasicLogger
 * @see MessageLogger
 * @see LogMessage
 * @see Message
 */
@MessageLogger(projectCode = "SIGNAL")
public interface SignalLogger extends BasicLogger {

	SignalLogger LOGGER = Logger.getMessageLogger(SignalLogger.class, SignalLogger.class.getPackage().getName());

	@LogMessage(level = TRACE)
	@Message(id = 1, value = "[Signal Dispatcher] ================ Start Initialization Process ================ [Signal Dispatcher]")
	void startSignalModuleInitializationProcess();
	
	@LogMessage(level = TRACE)
	@Message(id = 2, value = "[Signal Dispatcher] ================ Finish Initialization Process ================ [Signal Dispatcher]")
	void finishSignalModuleInitializationProcess();	
	
	@LogMessage(level = DEBUG)
	@Message(id = 3, value = "[Signal Dispatcher] ================ Start Debugging ================ [Signal Dispatcher]")
	void printStartSignalDebug();
	
	@LogMessage(level = DEBUG)
	@Message(id = 4, value = "[Signal Dispatcher] ================ Finish Debugging ================ [Signal Dispatcher]")
	void printFinishSignalDebug();		
		
	@LogMessage(level = DEBUG)
	@Message(id = 5, value = "[System Module] id = '%s', label = '%s', name = '%s', active = %s")
	void printSystemModule(String identifier, String label, String name, Boolean active);		

	@LogMessage(level = DEBUG)
	@Message(id = 6, value = "[Reglament Process] id = '%s', qualifier = '%s', name = '%s', active = %s")
	void printReglamentProcess(String identifier, String qualifier, String name, Boolean active);		

	@LogMessage(level = DEBUG)
	@Message(id = 7, value = "[Message Template] id = '%s', level = '%s', name = '%s', description = '%s'")
	void printMessageTemplate(String identifier, String level, String name, String description);		
	
	@LogMessage(level = ERROR)
	@Message(id = 8, value = "Не задан модуль-источник в управляющем сигнале: task.id = %s, task.qualifier = %s")
	void signalSourceUndefined(String taskId, String taskQualifier);
	
}