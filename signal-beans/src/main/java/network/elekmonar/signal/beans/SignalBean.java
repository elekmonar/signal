package network.elekmonar.signal.beans;

import java.io.Serializable;

public class SignalBean implements Serializable {

	private static final long serialVersionUID = 7996389358066896869L;
	
	private Long processId;
	
	/**
	 * Путь (URI) к получателю
	 */
	private String destination;

}