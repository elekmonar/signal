package network.elekmonar.signal.beans;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.UUID;

import network.elekmonar.signal.spec.ControlSignal;
import network.elekmonar.signal.spec.SignalParameter;

/**
 * Управляющий сигнал.
 * Используется для инициирования запуска какого-либо процесса.
 * Процессу присваивается номер (идентификатор), назначенный 
 * в этом сигнале. С этим идентификатором выполняющийся процесс
 * должен слать оповещающие сигналы о своём текущем статусе
 * и о ходе выполнения процесса. 
 * 
 * @author Vitaly Masterov
 * @since 0.1
 *
 */
public class ControlSignalBean implements ControlSignal, Serializable {

	private static final long serialVersionUID = -407593492056720805L;
	
	private Boolean reportStatus;
		
	private UUID process;
	
	/**
	 * Идентификатор узла автоматизации (в составе серверной группировки
	 * и находящемся на определённом объекте автоматизации)
	 */
	private UUID node;
	
	/**
	 * Идентификатор модуля-источника, где был
	 * изначально сформирован сигнал
	 */
	private String source;
	
	/**
	 * Является ли модуль, сформировавший управляющий сигнал,
	 * модулем UI, отвечающим за работу пользовательского интерфейса.
	 */
	private Boolean sourceUI;	
	
	/**
	 * Идентификатор пользователя, который
	 * сформировал заявку (в случае если инициатором
	 * управляющего сигнала был пользователь, а не процесс)
	 */
	private UUID user;
	
	/**
	 * Идентификатор задачи из реестра
	 * описанных задач всех модулей
	 */
	private String task;
	
	/**
	 * Приложение, которое должно исполнить задачу
	 */
	private String application;
	
	/**
	 * Дополнительные параметры, необходмиые для передачи
	 * в качестве аргументов запускаемому процессу.
	 * Параметры здесь складываются в виде ключ-значение.
	 */
	private Map<String, Object> parameters;
	
	public ControlSignalBean() {
		parameters = new HashMap<>();
		this.reportStatus = Boolean.TRUE;
	}
	
	public ControlSignalBean(String source, UUID user) {
		this();
		this.source = source;
		this.user = user;
	}
			
	@Override
	public Boolean getReportStatus() {
		return reportStatus;
	}

	@Override
	public void setReportStatus(Boolean reportStatus) {
		this.reportStatus = reportStatus;
	}
	
	public UUID getProcess() {
		return process;
	}

	public void setProcess(UUID process) {
		this.process = process;
	}

	@Override
	public UUID getNode() {
		return node;
	}

	@Override
	public void setNode(UUID node) {
		this.node = node;
	}

	@Override
	public String getSource() {
		return source;
	}

	@Override
	public void setSource(String source) {
		this.source = source;
	}
	
	@Override
	public Boolean getSourceUI() {
		return sourceUI;
	}

	public void setSourceUI(Boolean sourceUI) {
		this.sourceUI = sourceUI;
	}

	@Override
	public UUID getUser() {
		return user;
	}

	@Override
	public void setUser(UUID user) {
		this.user = user;
	}
		
	@Override
	public String getApplication() {
		return application;
	}

	@Override
	public void setApplication(String application) {
		this.application = application;
	}

	@Override
	public String getTask() {
		return task;
	}

	@Override
	public void setTask(String task) {
		this.task = task;
	}
		
	@Override
	@SuppressWarnings("unchecked")
	public <T> T getParameter(Class<T> parameterClass, String key) {
		if (!parameters.containsKey(key)) {
			// TODO throw Exception ... 
		}
		
		return (T)parameters.get(key);
	}

	@Override
	public void setParameter(String key, Object value) {
		parameters.put(key, value);
	}

	@Override
	public List<SignalParameter> getParameters() {
		List<SignalParameter> params = new ArrayList<>();
		Iterator<Entry<String, Object>> iter = parameters.entrySet().iterator();
		while (iter.hasNext()) {
			Entry<String, Object> entry = iter.next();
			SignalParameterBean spBean = new SignalParameterBean(entry.getKey(), entry.getValue());
			params.add(spBean);
		}

		return params;
	}
	
}