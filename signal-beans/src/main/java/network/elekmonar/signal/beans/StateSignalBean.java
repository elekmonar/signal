package network.elekmonar.signal.beans;

import java.io.Serializable;
import java.util.UUID;

import network.elekmonar.signal.spec.StateSignal;
import network.elekmonar.signal.spec.enums.ProcessState;

/**
 * Сообщение о статусе процесса
 * 
 * @author Vitaly Masterov
 * @since 0.1
 *
 */
public class StateSignalBean implements StateSignal, Serializable {

	private static final long serialVersionUID = -5824625264392074742L;
	
	/**
	 * Идентификатор процесса
	 */
	private UUID process;
	
	/**
	 * Текущее состояние процесса
	 */
	private ProcessState state;
	
	public StateSignalBean() {
		
	}

	public StateSignalBean(UUID process, ProcessState state) {
		this.process = process;
		this.state = state;
	}
	
	@Override
	public UUID getProcess() {
		return process;
	}

	public void setProcess(UUID process) {
		this.process = process;
	}

	@Override
	public ProcessState getState() {
		return state;
	}

	public void setState(ProcessState state) {
		this.state = state;
	}

}