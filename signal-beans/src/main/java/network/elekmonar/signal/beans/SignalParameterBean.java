package network.elekmonar.signal.beans;

import java.io.Serializable;

import network.elekmonar.signal.spec.SignalParameter;

/**
 * Параметр управляющего сигнала
 * 
 * @author Vitaly Masterov
 * @see SignalParameter
 * @since 0.1
 *
 */
public class SignalParameterBean implements SignalParameter, Serializable {

	private static final long serialVersionUID = -429024053275827420L;

	private String key;
	
	private Object value;
	
	public SignalParameterBean(String key, Object value) {
		this.key = key;
		this.value = value;		
	}
	
	@Override
	public String getKey() {
		return key;
	}

	@Override
	public Object getValue() {
		return value;
	}

}