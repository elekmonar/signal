package network.elekmonar.signal.beans;

import java.io.Serializable;
import java.util.UUID;

import network.elekmonar.signal.spec.ErrorSignal;

/**
 * Сигнал, который посылается в случае возникновения ошибки.
 * 
 * @author Vitaly Masterov
 * @since 0.1
 *
 */
public class ErrorSignalBean implements ErrorSignal, Serializable {

	private static final long serialVersionUID = -7380902070031400967L;
	
	/**
	 * Идентификатор процесса
	 */
	private UUID process;

	/**
	 * Код ошибки
	 */
	private String code;
	
	/**
	 * Сериализованный в {@link byte[]}
	 * java {@literal stack trace} исключения.
	 */
	private byte[] stackTrace;
	
	public ErrorSignalBean() {
		
	}

	public ErrorSignalBean(UUID process, String code) {
		this.process = process;
		this.code = code;
	}

	public ErrorSignalBean(UUID process, String code, byte[] stackTrace) {
		this.process = process;
		this.code = code;
		this.stackTrace = stackTrace;
	}
	
	public UUID getProcess() {
		return process;
	}

	public void setProcess(UUID process) {
		this.process = process;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	@Override
	public byte[] getStackTrace() {	
		return stackTrace;
	}

	@Override
	public void setStackTrace(byte[] stackTrace) {
		this.stackTrace = stackTrace;
	}

}