package network.elekmonar.signal.beans;

import java.io.Serializable;
import java.util.UUID;

import network.elekmonar.signal.spec.ProgressSignal;

/**
 * Сообщение о ходе состояния какого-либо процесса.
 * Используется для отображения на индикаторах.
 * {@literal value} лежит в пределах от нуля до ста процентов.
 * 
 * @author Vitaly Masterov
 * @since 0.1
 *
 */
public class ProgressSignalBean implements ProgressSignal, Serializable {

	private static final long serialVersionUID = -8568229892004744130L;
	
	/**
	 * Идентификатор процесса
	 */
	private UUID process;
	
	/**
	 * Значение прогресса
	 */
	private Integer value;
	
	public ProgressSignalBean() {
		
	}
	
	public ProgressSignalBean(UUID process, Integer value) {
		this.process = process;
		this.value = value;
	}
	
	public UUID getProcess() {
		return process;
	}

	public void setProcess(UUID process) {
		this.process = process;
	}

	public Integer getValue() {
		return value;
	}

	public void setValue(Integer value) {
		this.value = value;
	}

}